﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using VectorXD = MathNet.Numerics.LinearAlgebra.Vector<double>;
using MatrixXD = MathNet.Numerics.LinearAlgebra.Matrix<double>;
using DenseVectorXD = MathNet.Numerics.LinearAlgebra.Double.DenseVector;
using DenseMatrixXD = MathNet.Numerics.LinearAlgebra.Double.DenseMatrix;

public class Spring : MonoBehaviour {

    #region InEditorVariables

    public float Stiffness;
    public Node nodeA;
    public Node nodeB;

    #endregion

    public float Length0;
    public float Length;

    private PhysicsManager Manager;

    // Update is called once per frame
    void Update () {

        Vector3 yaxis = new Vector3(0.0f, 1.0f, 0.0f);
        Vector3 dir = nodeA.Pos - nodeB.Pos;
        dir.Normalize();

        transform.localPosition = 0.5f * (nodeA.Pos + nodeB.Pos);
        //The default length of a cylinder in Unity is 2.0
        transform.localScale = new Vector3(transform.localScale.x, Length / 2.0f, transform.localScale.z);
        transform.localRotation = Quaternion.FromToRotation(yaxis, dir);
	}

    // Use this for initialization
    public void Initialize(PhysicsManager m)
    {
        Manager = m;

        UpdateState();
        Length0 = Length;
    }

    // Update spring state
    public void UpdateState()
    {
        Length = (nodeA.Pos - nodeB.Pos).magnitude;
    }

    // Compute force and add to the nodes
    public void ComputeForce()
    {
        //Elastic force
        Vector3 Force = -Stiffness * (Length - Length0) * (nodeA.Pos - nodeB.Pos) / Length;
        nodeA.Force += Force;
        nodeB.Force -= Force;
    }

    public float ComputePotentialEnergy()
    {
        //spring potential energy: 1/2 * k * 1/r * (xnorm - r)^2
        float potentialEnergy = 0.5f * Stiffness * 1.0f / Length0 * Mathf.Pow(Length-Length0, 2.0f);

        return potentialEnergy;
    }

    // Get Force Jacobian
    public void GetForceJacobian(MatrixXD dFdx)
    {
        MatrixXD dFadx = new DenseMatrixXD(3);
        MatrixXD I = DenseMatrixXD.CreateIdentity(3);
        Vector3 u_aux = (nodeA.Pos - nodeB.Pos) / Length;
        VectorXD u = new DenseVectorXD(3);
        u[0] = u_aux[0];
        u[1] = u_aux[1];
        u[2] = u_aux[2];
        MatrixXD uuT = u.ToColumnMatrix() * u.ToRowMatrix();

        dFadx = -Stiffness * (Length - Length0) * ((1.0f / Length) * (I - uuT)) - Stiffness * uuT;

        CalculatePartialJacobian(dFdx,  dFadx, nodeA.index, nodeA.index); //FaXa
        CalculatePartialJacobian(dFdx, -dFadx, nodeA.index, nodeB.index); //FaXb
        CalculatePartialJacobian(dFdx, -dFadx, nodeB.index, nodeA.index); //FbXa
        CalculatePartialJacobian(dFdx,  dFadx, nodeB.index, nodeB.index); //FbXb
    }

    public void CalculatePartialJacobian(MatrixXD dFdx, MatrixXD dFadx, int i, int j)
    {
        dFdx[i,         j]  += dFadx[0, 0];
        dFdx[i,     j + 1]  += dFadx[0, 1];
        dFdx[i,     j + 2]  += dFadx[0, 2];
        dFdx[i + 1,     j]  += dFadx[1, 0];
        dFdx[i + 1, j + 1]  += dFadx[1, 1];
        dFdx[i + 1, j + 2]  += dFadx[1, 2];
        dFdx[i + 2,     j]  += dFadx[2, 0];
        dFdx[i + 2, j + 1]  += dFadx[2, 1];
        dFdx[i + 2, j + 2]  += dFadx[2, 2];
    }

 }
