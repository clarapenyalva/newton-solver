﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;

using VectorXD = MathNet.Numerics.LinearAlgebra.Vector<double>;
using MatrixXD = MathNet.Numerics.LinearAlgebra.Matrix<double>;
using DenseVectorXD = MathNet.Numerics.LinearAlgebra.Double.DenseVector;
using DenseMatrixXD = MathNet.Numerics.LinearAlgebra.Double.DenseMatrix;

/// <summary>
/// Basic physics manager capable of simulating a given ISimulable
/// implementation using diverse integration methods: explicit,
/// implicit, Verlet, semi-implicit and implicit(Newton)
/// </summary>
public class PhysicsManager : MonoBehaviour
{
    /// <summary>
    /// Default constructor. Zero all. 
    /// </summary>
    public PhysicsManager()
    {        
        Paused = true;
        TimeStep = 0.01f;
        Gravity = new Vector3(0.0f, -9.81f, 0.0f);
        IntegrationMethod = Integration.Newton;
        frames = 0;
    }

    /// <summary>
    /// Integration method.
    /// </summary>
    public enum Integration
    {
        Explicit = 0,
        Symplectic = 1,
        Midpoint = 2,
        Verlet = 3,
        Implicit = 4,
        Newton = 5,
    };//

    private string pathWriteTextTest, pathWriteTextTestEnergy, pathWriteTextTestEnPeriod;
    private string pathWriteTextTopology, pathWriteTextDynamic;
    private string pathWriteTextProcessing;

    private StreamWriter sw_dynamic, sw_topology, sw_dataTestEnergy, sw_dataTestEnergyPeriod, sw_dataTest, sw_dataProcessing;

    public string codeTest = "CatFix10_0-01-1e6-3"; //code-solver-dt-stiff-it

    #region InEditorVariables

    public bool Paused;
    public float TimeStep;
    public float iterationsNewton;
    public Vector3 Gravity;
    public List<GameObject> SimObjects;
    public Integration IntegrationMethod;

    #endregion

    #region OtherVariables
    private List<ISimulable> m_objs;
    private int m_numDoFs;
    private bool first;
    private VectorXD xOld;
    private int frames;
    private float currentTime = 0.0f;
    
    //Energy variables
    private bool stopSimulation = false;
    private bool goingUp = true;
    private float peak = 0.0f;
    private float prevEnergy = 0.0f;
    private float energy;
    private float slope;
    private float thresholdEnergy = 0.001f;
    private float kineticEnergyPeriod = 0.0f;
    private int countKineticEnergyPeriod = 0;
    private float sumKineticEnergyPeriod = 0.0f;
    private float timePrevPeak = 0.0f;

    private int solverIterations = 0;
    private int totalPeakKinetic = 0;


    private double computTime = 0.0;
    private Stopwatch timePerParse;
    #endregion

    #region MonoBehaviour

    public void Start()
    {
        Application.runInBackground = true;
        //Parse the simulable objects and initialize their state indices
        m_numDoFs = 0;
        m_objs = new List<ISimulable>(SimObjects.Capacity);

        first = true; //Used in verlet integration

        foreach (GameObject obj in SimObjects)
        {
            ISimulable simobj = obj.GetComponent<ISimulable>();
            if (simobj != null)
            {
                m_objs.Add(simobj);

                // Initialize simulable object
                simobj.Initialize(m_numDoFs, this);

                // Retrieve pos and vel size
                m_numDoFs += simobj.GetNumDoFs();
            }
        }

        //WRITE data
        string appPathDTestVisualProc = Directory.GetCurrentDirectory() + "\\DataTests\\" + codeTest;
        string appPathDTestVisualBlend = Directory.GetCurrentDirectory() + "\\DataTests\\" + codeTest + "\\" + codeTest + "_visual";
        string appPathDTest = Directory.GetCurrentDirectory() + "\\DataTests\\" + codeTest + "\\" + codeTest + "_data";

        if (!Directory.Exists(appPathDTestVisualProc))
        {
            Directory.CreateDirectory(appPathDTestVisualProc);
        }

        if (!Directory.Exists(appPathDTestVisualBlend))
        {
            Directory.CreateDirectory(appPathDTestVisualBlend);
        }


        if (!Directory.Exists(appPathDTest))
        {
            Directory.CreateDirectory(appPathDTest);
        }

        pathWriteTextTest = appPathDTest + "\\dataTest_";
        pathWriteTextTestEnergy = appPathDTest + "\\dataTestEnergy_";
        pathWriteTextTestEnPeriod = appPathDTest + "\\dataTestEnergyPeriod_";

        pathWriteTextProcessing = appPathDTestVisualProc + "\\posProcessing_";

        pathWriteTextTopology = appPathDTestVisualBlend + "\\topology_";
        pathWriteTextDynamic = appPathDTestVisualBlend + "\\positions_";

        writeDataTopology();
        writeDataDynamic();

        sw_dataTestEnergy = new StreamWriter(pathWriteTextTestEnergy + codeTest + ".dtest");
        sw_dataTestEnergy.WriteLine("time;kinetic;potential;gravPotential");

        sw_dataTestEnergyPeriod = new StreamWriter(pathWriteTextTestEnPeriod + codeTest + ".dtest");
        sw_dataTestEnergyPeriod.WriteLine("time;solverIter;kinetic;T");

        MassSpring massSpring = SimObjects[0].GetComponent<MassSpring>();
        sw_dataProcessing = new StreamWriter(pathWriteTextProcessing + codeTest + ".dproc");
        //1st line: typeSolver(0), deltaTime, iterations, numNodes, numSprings, mass/particle, stiffness
        //sw_dataProcessing.WriteLine("0;dt;iter/step;nodes;springs;mass/n;stiff");
        sw_dataProcessing.WriteLine(0 + ";" + TimeStep + ";" + iterationsNewton + ";" + massSpring.Nodes.Count + ";" + massSpring.Springs.Count + ";" + massSpring.Nodes[0].Mass + ";" + massSpring.Springs[0].Stiffness);

        //END WRITE data
    }

    void OnApplicationQuit()
    {
        UnityEngine.Debug.Log("Application ending after " + Time.time + " seconds");
    }

    public void Update()
    {
        if (Input.GetKeyUp(KeyCode.P))
            this.Paused = !this.Paused;
    }

    public void FixedUpdate()
    {
        if (this.Paused || stopSimulation == true)
            return; // Not simulating

        timePerParse = Stopwatch.StartNew();

        // Select integration method
        switch (this.IntegrationMethod)
        {
            case Integration.Explicit: this.stepExplicit(); break;
            case Integration.Symplectic: this.stepSymplectic(); break;
            case Integration.Midpoint: this.stepMidpoint(); break;
            case Integration.Verlet: this.stepVerlet(); break;
            case Integration.Implicit: this.stepImplicit(); break;
            case Integration.Newton: this.stepNewton(); break;
            default:
                throw new System.Exception("[ERROR] Should never happen!");
        }

        timePerParse.Stop();
        computTime += timePerParse.ElapsedMilliseconds / 1000.0;

        currentTime += TimeStep;
        solverIterations++;
        
        writeDataDynamic();
        writeDataTestEnergy();

        writeDataPosProcessing();

        if (stopSimulation)
        {
            writeDataTest();
            sw_dataTestEnergyPeriod.Close();
            sw_dataTestEnergy.Close();
            sw_dataProcessing.Close();
            UnityEngine.Debug.Log("STOP SIMULATION: " + currentTime + "s");
            UnityEngine.Debug.Log("TIME: " + computTime + "s");

            Application.Quit();
        }

        frames++;
        //Debug.Log(frames);   
    }

    #endregion

    private void writeDataTopology()
    {
        sw_topology = new StreamWriter(pathWriteTextTopology+codeTest+".top");
        //WRITE data (first lines, 2 random numbers, nodes)
        sw_topology.WriteLine("0");
        sw_topology.WriteLine("1");
        sw_topology.WriteLine(m_numDoFs / 3);

        //CATENARIA FIJA 10 NODOS
        sw_topology.WriteLine("1");
        sw_topology.WriteLine("1");
        sw_topology.WriteLine("2");
        sw_topology.WriteLine("0");
        sw_topology.WriteLine("2");
        sw_topology.WriteLine("2");
        sw_topology.WriteLine("1");
        sw_topology.WriteLine("3");
        sw_topology.WriteLine("2");
        sw_topology.WriteLine("2");
        sw_topology.WriteLine("4");
        sw_topology.WriteLine("2");
        sw_topology.WriteLine("3");
        sw_topology.WriteLine("5");
        sw_topology.WriteLine("2");
        sw_topology.WriteLine("4");
        sw_topology.WriteLine("6");
        sw_topology.WriteLine("2");
        sw_topology.WriteLine("5");
        sw_topology.WriteLine("7");
        sw_topology.WriteLine("2");
        sw_topology.WriteLine("6");
        sw_topology.WriteLine("8");
        sw_topology.WriteLine("2");
        sw_topology.WriteLine("7");
        sw_topology.WriteLine("9");
        sw_topology.WriteLine("1");
        sw_topology.WriteLine("8");

        sw_topology.Close();
    }

    private void writeDataDynamic()
    {
        VectorXD x = new DenseVectorXD(m_numDoFs);

        foreach (ISimulable obj in m_objs)
            obj.GetPosition(x);
               
        string ct = currentTime.ToString();
        if (!ct.Contains("."))
            ct += ".";

        sw_dynamic = new StreamWriter(pathWriteTextDynamic + codeTest +"_" + ct.PadRight(10, '0') + ".pos");
        //WRITE data (first lines, 2 random numbers, nodes)
        sw_dynamic.WriteLine("0");
        sw_dynamic.WriteLine("1");
        sw_dynamic.WriteLine(m_numDoFs/3);

        for (int i = 0; i < x.Count; i++)
            sw_dynamic.WriteLine(x[i]);

        sw_dynamic.Close();
    }

    private void writeDataTestEnergy()
    {
        //TODO: make it prettier
        //Stop condition: the kinetics find a very low peak
        Vector3 energy = new Vector3();

        foreach (ISimulable obj in m_objs)
        {
            energy = obj.GetEnergy(); //node kinetic, spring potential, gravitational potential
            //TODO: change, different objects
        }

      
        slope = (energy[0] - prevEnergy) / TimeStep;

        if (slope < 0.0f && goingUp)
        {
            kineticEnergyPeriod = currentTime - timePrevPeak;
            countKineticEnergyPeriod++;
            sumKineticEnergyPeriod += kineticEnergyPeriod;
            timePrevPeak = currentTime;
            peak = prevEnergy;

            totalPeakKinetic++;
            goingUp = false;

            //currentTime, number of solver iterations, kinetic energy, T
            sw_dataTestEnergyPeriod.WriteLine(currentTime + ";" + solverIterations + ";" + energy[0] + ";" + kineticEnergyPeriod);

            if (peak < thresholdEnergy)
            {
                stopSimulation = true;
            }
        }
        else if (slope > 0.0f && !goingUp)
            goingUp = true;

        prevEnergy = energy[0];

        //node kinetic, spring potential, gravitational potential
        sw_dataTestEnergy.WriteLine(currentTime + ";" + energy[0] + ";" + energy[1] + ";" + energy[2]);

        //Debug.Log("kinetic: " + energy[0]);
    }

    private void writeDataTest()
    {
        MassSpring massSpring = SimObjects[0].GetComponent<MassSpring>();
        sw_dataTest = new StreamWriter(pathWriteTextTest+codeTest + ".dtest");


        float TkineticEnAvg = sumKineticEnergyPeriod / countKineticEnergyPeriod;

        sw_dataTest.WriteLine("idSolver;dt;stiff;iter/step;totalSolverIter;error;totalPeakKinetic;Tavg");
        sw_dataTest.WriteLine(0 + ";" + TimeStep + ";" + massSpring.Springs[0].Stiffness + ";" + iterationsNewton + ";" + solverIterations + ";" + 0 + ";" + totalPeakKinetic + ";" + TkineticEnAvg);

        sw_dataTest.WriteLine("nodes;springs;mass/n");
        sw_dataTest.WriteLine(massSpring.Nodes.Count + ";" + massSpring.Springs.Count + ";" + massSpring.Nodes[0].Mass);

        sw_dataTest.WriteLine("FinalPos");
        VectorXD x = new DenseVectorXD(m_numDoFs);
        foreach (ISimulable obj in m_objs)
            obj.GetPosition(x);
         for (int i = 0; i < x.Count; i++)
            sw_dataTest.WriteLine(x[i]);

        sw_dataTest.Close();

    }

    private void writeDataPosProcessing()
    {       
        VectorXD x = new DenseVectorXD(m_numDoFs);
        foreach (ISimulable obj in m_objs)
            obj.GetPosition(x);
        int id = 0;
        for (int i = 0; i <= x.Count - 3; i += 3)
        {
            sw_dataProcessing.WriteLine(x[i] + ";" + x[i + 1] + ";" + x[i + 2] + ";" + id);
            id++;
        }
    }

    /// <summary>
    /// Performs a simulation step using Explicit integration.
    /// </summary>
    private void stepExplicit()
    {
        VectorXD x = new DenseVectorXD(m_numDoFs);
        VectorXD v = new DenseVectorXD(m_numDoFs);
        VectorXD f = new DenseVectorXD(m_numDoFs);
        MatrixXD Minv = new DenseMatrixXD(m_numDoFs);
        Minv.Clear();

        foreach (ISimulable obj in m_objs)
        {
            obj.GetPosition(x);
            obj.GetVelocity(v);
            obj.ComputeForces();
            obj.GetForce(f);
            obj.GetMassInverse(Minv);
        }

        foreach (ISimulable obj in m_objs)
        {
            obj.FixVector(f);
            obj.FixMatrix(Minv);
        }

        x += TimeStep * v;
        v += TimeStep * (Minv * f);

        foreach (ISimulable obj in m_objs)
        {
            obj.SetPosition(x);
            obj.SetVelocity(v);
        }
    }

    /// <summary>
    /// Performs a simulation step using Symplectic integration.
    /// </summary>
    private void stepSymplectic()
    {
        VectorXD x = new DenseVectorXD(m_numDoFs);
        VectorXD v = new DenseVectorXD(m_numDoFs);
        VectorXD f = new DenseVectorXD(m_numDoFs);
        MatrixXD Minv = new DenseMatrixXD(m_numDoFs);
        Minv.Clear();

        foreach (ISimulable obj in m_objs)
        {
            obj.GetPosition(x);
            obj.GetVelocity(v);
            obj.ComputeForces();
            obj.GetForce(f);
            obj.GetMassInverse(Minv);
        }

        foreach (ISimulable obj in m_objs)
        {
            obj.FixVector(f);
            obj.FixMatrix(Minv);
        }

        v += TimeStep * (Minv * f);
        x += TimeStep * v;

        foreach (ISimulable obj in m_objs)
        {
            obj.SetPosition(x);
            obj.SetVelocity(v);
        }
    }

    /// <summary>
    /// Performs a simulation step using Midpoint integration.
    /// </summary>
    private void stepMidpoint()
    {
        VectorXD x = new DenseVectorXD(m_numDoFs);
        VectorXD v = new DenseVectorXD(m_numDoFs);
        VectorXD f = new DenseVectorXD(m_numDoFs);
        MatrixXD Minv = new DenseMatrixXD(m_numDoFs);
        Minv.Clear();

        foreach (ISimulable obj in m_objs)
        {
            obj.GetPosition(x);
            obj.GetVelocity(v);
            obj.ComputeForces();
            obj.GetForce(f);
            obj.GetMassInverse(Minv);
        }

        foreach (ISimulable obj in m_objs)
        {
            obj.FixVector(f);
            obj.FixMatrix(Minv);
        }

        VectorXD x0 = x;
        VectorXD v0 = v;

        //Integrate to h/2
        x += 0.5f * TimeStep * v;
        v += 0.5f * TimeStep * (Minv * f);

        //update state
        foreach (ISimulable obj in m_objs)
        {
            obj.SetPosition(x);
            obj.SetVelocity(v);
        }

        //Compute forces at h/2
        foreach (ISimulable obj in m_objs)
        {
            obj.ComputeForces();
            obj.GetForce(f);
            obj.GetMassInverse(Minv);
        }

        foreach (ISimulable obj in m_objs)
        {
            obj.FixVector(f);
            obj.FixMatrix(Minv);
        }

        //Integrate to h
        x = x0 + TimeStep * v;
        v = v0 + TimeStep * (Minv * f);

        foreach (ISimulable obj in m_objs)
        {
            obj.SetPosition(x);
            obj.SetVelocity(v);
        }
    }

    /// <summary>
    /// Performs a simulation step using Verlet integration.
    /// </summary>
    private void stepVerlet()
    {
        VectorXD x = new DenseVectorXD(m_numDoFs);
        VectorXD v = new DenseVectorXD(m_numDoFs);
        VectorXD f = new DenseVectorXD(m_numDoFs);
        MatrixXD Minv = new DenseMatrixXD(m_numDoFs);
        Minv.Clear();

        foreach (ISimulable obj in m_objs)
        {
            obj.GetPosition(x);
            obj.GetVelocity(v);
            obj.ComputeForces();
            obj.GetForce(f);
            obj.GetMassInverse(Minv);
        }

        foreach (ISimulable obj in m_objs)
        {
            obj.FixVector(f);
            obj.FixMatrix(Minv);
        }

        VectorXD x0 = x;

        if (first)
        {
            x = x + TimeStep * v + 0.5f * TimeStep * TimeStep * (Minv * f);
            first = false;
        }
        else
            x = 2 * x - xOld + TimeStep * TimeStep * (Minv * f);

        v = (1.0f / TimeStep) * (x - x0);

        xOld = x0;

        foreach (ISimulable obj in m_objs)
        {
            obj.SetPosition(x);
            obj.SetVelocity(v);
        }
    }

    /// <summary>
    /// Performs a simulation step using Semi-implicit integration.
    /// </summary>
    private void stepImplicit()
    {       
        VectorXD x = new DenseVectorXD(m_numDoFs);
        VectorXD v = new DenseVectorXD(m_numDoFs);
        VectorXD f = new DenseVectorXD(m_numDoFs);
        MatrixXD M = new DenseMatrixXD(m_numDoFs);
        MatrixXD dFdx = new DenseMatrixXD(m_numDoFs);
        MatrixXD dFdv = new DenseMatrixXD(m_numDoFs);
        M.Clear();
        dFdx.Clear();
        dFdv.Clear();

        foreach (ISimulable obj in m_objs)
        {
            obj.GetPosition(x);
            obj.GetVelocity(v);
            obj.ComputeForces();//se computan las fuerzas
            obj.GetForce(f);
            obj.GetForceJacobian(dFdx);
            obj.GetMass(M);
        }

        //Sistema de ecuaciones a resolver:
        //M*vi+1 = M*Vi + h*F(xi+1, vi+1)
        //xi+1 = xi + h*Vi+1

        //Se crea el sistema lineal para calcular la velocidad:
        //b = M * v + TimeStep * f - TimeStep * dFv * vi
        //A = M - TimeStep * dFdv - TimeStep*TimeStep * dFdx   
        //vi+1 = A.Solve(b)

        //Con la velocidad, se resuelve la posición:
        //x = x + TimeStep * vi+1

        //dFdv -> fuerzas que dependen de la velocidad (damping)
        //dFdx -> fuerzas que dependen de la posición (elasticidad)
        MatrixXD A = M - TimeStep * dFdv - (TimeStep * TimeStep) * dFdx;
        VectorXD b = (M - TimeStep * dFdv) * v + TimeStep * f;

        //Se añade esta fijación para que en el caso de que TimeStep = 1, 
        //la matriz A no sea 0 (la diagonal) para los nodos fijados
        //Así el método Solve() no da error
        foreach (ISimulable obj in m_objs)
        {
            obj.FixVector(b);
            obj.FixMatrix(A);
        }

        //se resuelve el sistema y se calculan las nuevas velocidades y posiciones
        v = A.Solve(b);
        x += TimeStep * v;

        foreach (ISimulable obj in m_objs)
        {
            obj.SetPosition(x);
            obj.SetVelocity(v);
        }
    }

    /// <summary>
    /// Performs a simulation step using Implicit integration.
    /// </summary>
    private void stepNewton()
    {
        VectorXD v0 = new DenseVectorXD(m_numDoFs);
        VectorXD x0 = new DenseVectorXD(m_numDoFs);
        MatrixXD M = new DenseMatrixXD(m_numDoFs);
        M.Clear();
        foreach (ISimulable obj in m_objs)
        {
            obj.GetVelocity(v0);
            obj.GetPosition(x0);
            obj.GetMass(M);
        }
        stepImplicit();

        for (int i = 0; i < iterationsNewton; i++)
        {
            VectorXD x = new DenseVectorXD(m_numDoFs);
            VectorXD v = new DenseVectorXD(m_numDoFs);
            VectorXD f = new DenseVectorXD(m_numDoFs);
            MatrixXD dfdx = new DenseMatrixXD(m_numDoFs);
            dfdx.Clear();

            foreach (ISimulable obj in m_objs)
            {
                obj.GetPosition(x);
                obj.GetVelocity(v);
                obj.ComputeForces();
                obj.GetForce(f);
                obj.GetForceJacobian(dfdx);
            }

            MatrixXD A = M - TimeStep * TimeStep * dfdx;
            VectorXD b = M * (v0 - v) + TimeStep * f;

            foreach (ISimulable obj in m_objs)
            {
                obj.FixVector(b);
                obj.FixMatrix(A);
            }

            VectorXD dv = A.Solve(b);
            v += dv;
            x = x0 + TimeStep * v;

            foreach (ISimulable obj in m_objs)
            {
                obj.SetPosition(x);
                obj.SetVelocity(v);
            }
        }

    }
}